# <h1 align="center">Transfer Order to Active Order DB</h1>
 
<!-- ABOUT THE PROJECT -->
## About The Project
 
This Project is Built using django channels. when the user made an order that order is Transfer to the active order database in some time of interval and removed from order table.
 
### Built With
 
* [Python](https://www.python.org/)
* [Django](https://www.djangoproject.com/)
 
### Installation
 
1. Download or clone the Repository to your device
2. Create Virtual Environment using `python3 -m venv <your environment name>`
3. Activate Virtual Environment for Mac and Linux user `source <virtual env path>/bin/activate`
4. Activate Virtual Environment for Windows user `venv\Scripts\activate`
5. type `pip install -r requirements.txt` (this will install required package for project)
6. type `python3 manage.py makemigrations`
7. type `python3 manage.py migrate`
8. type `python3 manage.py runserver`
