from channels.generic.websocket import AsyncJsonWebsocketConsumer
import json, random
from api.order.models import Order
from .models import ActiveDB
import time
from asgiref.sync import sync_to_async
import asyncio


class TableData(AsyncJsonWebsocketConsumer):
    async def connect(self):
        self.group_name = "tableData"
        await self.channel_layer.group_add(self.group_name, self.channel_name)
        await self.accept()

    async def disconnect(self, close_code):
        pass

    @sync_to_async
    def foo(self):
        order = Order.objects.values_list("product_names", flat=True)
        print(order)
        for i in order:
            ActiveDB.objects.create(product_name=i)
            x = Order.objects.filter(product_names=i)
            x.delete()

        active_order = ActiveDB.objects.all()
        print(active_order)

    async def receive(self, text_data):
        while True:
            await self.foo()
            await asyncio.sleep(30)

        # for i in active_order:
        #     print(i.product_name)

        # listOfIndex = ["stock1", "stock2", "stock3"]

        # for i in range(100):
        #     import time

        #     time.sleep(5)
        #     pp = json.dumps(
        #         {
        #             "indexName": random.choice(listOfIndex),
        #             "value": random.randint(1, 1000),
        #         }
        #     )
        #     print(pp)
        #     random.choice(listOfIndex)