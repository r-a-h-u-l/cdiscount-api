from django.db import models


class ActiveDB(models.Model):
    product_name = models.CharField(max_length=150)

    def __str__(self):
        return self.product_name
