from django.urls import path, include
from . import views

urlpatterns = [
    path('',views.index,name='index'),
    path("product/", include("api.product.urls")),
    path("order/", include("api.order.urls")),
    path("user/", include("api.user.urls")),
]