from django.urls import re_path


from . import consumer

ws_pattern = [re_path("ws/tableData/", consumer.TableData.as_asgi())]